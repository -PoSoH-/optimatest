
package ua.i.serhii.stacktest.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("house_number")
    @Expose
    private String houseNumber;
    @SerializedName("road")
    @Expose
    private String road;
    @SerializedName("neighbourhood")
    @Expose
    private String neighbourhood;
    @SerializedName("suburb")
    @Expose
    private String suburb;
    @SerializedName("county")
    @Expose
    private String county;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("postcode")
    @Expose
    private String postcode;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("country_code")
    @Expose
    private String countryCode;

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getNeighbourhood() {
        return neighbourhood;
    }

    public void setNeighbourhood(String neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAllAddress(){
        StringBuilder result = new StringBuilder();

        result.append('\n');
        result.append("house_number : ");  result.append(houseNumber);   result.append('\n');
        result.append("road : ");          result.append(road);          result.append('\n');
        result.append("neighbourhood : "); result.append(neighbourhood); result.append('\n');
        result.append("suburb : ");        result.append(suburb); result.append('\n');
        result.append("county : ");        result.append(county); result.append('\n');
        result.append("city : ");          result.append(city); result.append('\n');
        result.append("postcode : ");      result.append(postcode); result.append('\n');
        result.append("country : ");       result.append(country); result.append('\n');
        result.append("country_code : ");  result.append(countryCode); result.append('\n');

        return result.toString();
    }

}
