
package ua.i.serhii.stacktest.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocalResult {

    @SerializedName("place_id")
    @Expose
    private Integer placeId;
    @SerializedName("licence")
    @Expose
    private String licence;
    @SerializedName("osm_type")
    @Expose
    private String osmType;
    @SerializedName("osm_id")
    @Expose
    private Integer osmId;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("boundingbox")
    @Expose
    private List<String> boundingbox = null;

    public Integer getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Integer placeId) {
        this.placeId = placeId;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getOsmType() {
        return osmType;
    }

    public void setOsmType(String osmType) {
        this.osmType = osmType;
    }

    public Integer getOsmId() {
        return osmId;
    }

    public void setOsmId(Integer osmId) {
        this.osmId = osmId;
    }

    public String getLat() {
        return lat;
    }
    public Double getLatitude() {
        return Double.valueOf(lat);
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public Double getLongitude() {
        return Double.valueOf(lon);
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<String> getBoundingbox() {
        return boundingbox;
    }

    public void setBoundingbox(List<String> boundingbox) {
        this.boundingbox = boundingbox;
    }


    public String getAllInfo(){
        StringBuilder result = new StringBuilder();
        result.append("place_id : ");     result.append(placeId);     result.append('\n');
        result.append("licence : ");      result.append(licence);     result.append('\n');
        result.append("osm_type : ");     result.append(osmType);     result.append('\n');
        result.append("osm_id : ");       result.append(osmId);       result.append('\n');
        result.append("latitude  : ");    result.append(lat);         result.append('\n');
        result.append("longitude : ");    result.append(lon);         result.append('\n');
        result.append("display_name : "); result.append(displayName); result.append('\n');
        result.append("address");         result.append(address.getAllAddress());     result.append('\n');
        result.append("boundingbox");     result.append(boundingbox); result.append('\n');
        return result.toString();
    }

}
