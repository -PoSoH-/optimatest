package ua.i.serhii.stacktest.interfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ua.i.serhii.stacktest.models.LocalResult;

public interface LoadLocalResult {

//    @GET(BackendHelper.URL_CLASS_REVERSE
//            + "?"
//            + BackendHelper.URL_FORMAT
//            + "&"
//            + BackendHelper.URL_LAT
//            + "{lat}"
//            + "&"
//            + BackendHelper.URL_LON
//            + "{lon}")
//    Call<LocalResult> getLocal(@Path("lat") String lat, @Path("lon") String lon);

    @GET("{reverse}")
    Call<LocalResult> getLocal(@Path("reverse") String reverse
            , @Query("format") String format
            , @Query("lat") String lat
            , @Query("lon") String lon);

}
