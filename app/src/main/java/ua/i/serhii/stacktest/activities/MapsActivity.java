package ua.i.serhii.stacktest.activities;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.i.serhii.stacktest.App;
import ua.i.serhii.stacktest.R;
import ua.i.serhii.stacktest.helpers.BackendHelper;
import ua.i.serhii.stacktest.interfaces.LoadLocalResult;
import ua.i.serhii.stacktest.models.LocalResult;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LocalResult localResult;
    private LocationManager locationManager;
    private boolean isNetworkEnable = false;

    private TextView gpsStatus;
    private TextView netStatus;
    private TextView info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        gpsStatus = (TextView) findViewById(R.id.txt_state_gps);
        netStatus = (TextView) findViewById(R.id.txt_state_network);
        info = (TextView) findViewById(R.id.txt_other_info);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume() {
        super.onResume();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                startManagerGPS();
                startManagerNetvork();
            }else{
                ActivityCompat.requestPermissions(this,
                        new String[] {Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION},
                        0);
            }
        }else {
            startManagerGPS();
            startManagerNetvork();
        }
    }

    private void changeState(){
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            gpsStatus.setText(getString(R.string.txt_provider_gps_enb));
            netStatus.setText(getString(R.string.txt_provider_net_dis));
        }else if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            gpsStatus.setText(getString(R.string.txt_provider_gps_dis));
            netStatus.setText(getString(R.string.txt_provider_net_enb));
        }else{
            gpsStatus.setText(getString(R.string.txt_provider_gps_dis));
            netStatus.setText(getString(R.string.txt_provider_net_dis));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(locationListener);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        || grantResults[1] == PackageManager.PERMISSION_GRANTED){
                    startManagerNetvork();
                    startManagerGPS();
                } else{
                    Toast.makeText(this, "Not enabled manager location", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void startManagerGPS(){
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 1000 * 1, 10, locationListener);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void startManagerNetvork(){
        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 1000 * 1, 10, locationListener);
        }
    }

    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            loadLocalResult(location.getLatitude(), location.getLongitude());
            changeState();
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    private void updateMap(){
        LatLng newAddress = new LatLng(localResult.getLatitude(), localResult.getLongitude());
        mMap.addMarker(new MarkerOptions().position(newAddress).title(localResult.getAddress().getRoad()));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target( new LatLng(localResult.getLatitude(), localResult.getLongitude()))
                .zoom(18)
                .bearing(0)
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        info.setText(localResult.getAllInfo());
    }

    private void loadLocalResult(final Double lan, final Double lon){

        App.getBaseConnect().create(LoadLocalResult.class).getLocal(BackendHelper.URL_CLASS_REVERSE
                , BackendHelper.URL_FORMAT
                , String.valueOf(lan) //"50.50789121415715"
                , String.valueOf(lon) /*"30.44749259948731"*/ ).enqueue(new Callback<LocalResult>() {
            @Override
            public void onResponse(Call<LocalResult> call, Response<LocalResult> response) {
                if(response.isSuccessful()){
                    localResult = response.body();
                    updateMap();
                    Log.e("SUCCES", response.body().toString());
                }else{
                    Log.e("ERROR", response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<LocalResult> call, Throwable t) {
                Log.e("FAILTURE", call.toString());
            }
        });

    }

}
